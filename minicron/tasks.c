#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <fcntl.h>
#include <time.h>
#include "tasks.h"

void addTask(Task** tasks, int h, int min, char* com, int mode)
{
    Task* new_task = (Task*)malloc(sizeof(Task));

    new_task->hour=h;
    new_task->minute=min;
    new_task->command=com;
    new_task->mode=mode;
    new_task->next=NULL;

	Task *wsk;

    while (*tasks != NULL)
        tasks = &((*tasks)->next);

    *tasks = new_task;
}

void printTasks(Task* wsk)
{
	printf("=== TASKS LIST ===\n");
    while(wsk != NULL)
    {
        printf("%d:%d - %s\n", wsk->hour, wsk->minute, wsk->command);
        wsk = wsk->next;
    }
	printf("==================\n\n\n");
}

void sort(Task** tasks)
{
    struct Task* root = *tasks;
    struct Task* temp = NULL;
    struct Task* help = NULL;
    int hour, minute, mode, weight_temp, weight_help;
    char* command;
 
    for(temp = root; temp != NULL; temp = temp->next)
    {
        for(help = temp; help != NULL; help = help->next)
        {
            weight_temp = 100*temp->hour+temp->minute;
            weight_help = 100*help->hour+help->minute;
            if(weight_help < weight_temp)
            {
                hour = help->hour;
                help->hour = temp->hour;
                temp->hour = hour;
 
                minute = help->minute;
                help->minute = temp->minute;
                temp->minute = minute;
 
                command = help->command;
                help->command = temp->command;
                temp->command = command;
 
                mode = help->mode;
                help->mode = temp->mode;
                temp->mode = mode;
            }
        }
    }
   
    //pobranie aktualnego czasu
    time_t now;
    struct tm *now_tm;
    now = time(NULL);
    now_tm = localtime(&now);
 
    //przesuniecie zadan na kolejna dobe
    int weight_time_now = 100*now_tm->tm_hour+now_tm->tm_min;
    int weight_time_root = 100*(*tasks)->hour+(*tasks)->minute;
 
    while(weight_time_now > weight_time_root)
    {
        addTask(&(*tasks), (*tasks)->hour, (*tasks)->minute, (*tasks)->command, (*tasks)->mode);
        removeFirstElement(&(*tasks));
        weight_time_root = 100*(*tasks)->hour+(*tasks)->minute;
    }
}

void removeFirstElement(Task** tasks)
{
    struct Task *help = (*tasks);
    (*tasks) = (*tasks)->next;
    free(help);
}

void removeAllTasks(Task** tasks) {
    struct Task * help;
	while((*tasks) != NULL) {
		help = (*tasks);
		(*tasks) = (*tasks)->next;
		free(help);
	}
}

void readTaskFile(Task** tasks, char* fileName)
{
	int taskFile = open(fileName, O_RDONLY);
	if(taskFile == -1) {
		fprintf(stderr, "Nie udalo sie otworzyc pliku <taskfile> (%s).\n", fileName);
		return;
	}
	
	unsigned char line[128];
	unsigned char buff[1];
	size_t bytes_read;
	int step = 0;
	int line_i = 0;
	Task new_task;

	do {
		bytes_read = read(taskFile, buff, sizeof(buff));
		if(buff[0] == 10 && line_i > 0) {
			line[line_i++] = '\0';

			//parsowanie linii
			char * pch = strtok (line,":");
			while (pch != NULL) {
				
				if(pch != NULL) {
					switch(step) {
						
						case 0:
							sscanf(pch, "%d", &new_task.hour);
						break;
						case 1:
							sscanf(pch, "%d", &new_task.minute);
						break;
						case 2:
							new_task.command = malloc(sizeof(char) * strlen(pch));
							strcpy(new_task.command, pch);
						break;
						case 3:
							sscanf(pch, "%d", &new_task.mode);
						break;

					}
					step++;
				}
				
				pch = strtok (NULL,":");
			}
			
			addTask(&(*tasks), new_task.hour, new_task.minute, new_task.command, new_task.mode);
			
			step = 0;
			line_i = 0;
			
		} else {
			
			line[line_i++] = buff[0];
		}
		
	} while(bytes_read != 0);
	close(taskFile);
}

void printTasksToSyslog(Task* wsk)
{
	syslog(LOG_NOTICE, "=== TASKS TO DO: ===");
	int i = 1;
    while(wsk != NULL)
    {
        syslog(LOG_NOTICE, "%d. %d:%d - %s\n", i, wsk->hour, wsk->minute, wsk->command);
        wsk = wsk->next;
		i++;
    }
	syslog(LOG_NOTICE, "==================");
}