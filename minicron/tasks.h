#ifndef TASKS_H
#define TASKS_H

typedef struct Task {
	int hour;
	int minute;
	char *command;
	int mode;
	struct Task* next;
} Task;

void addTask(Task** tasks, int h, int min, char* com, int mode);
void printTasks(Task* tasks);
void sort(Task** tasks);
void removeFirstElement(Task** tasks);
void removeAllTasks(Task** tasks);
void readTaskFile(Task** tasks, char* fileName);
void printTasksToSyslog(Task* tasks);

#endif