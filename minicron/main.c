#include <stdio.h>
#include <fcntl.h> 
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <syslog.h>
#include "cron.h"
#include "tasks.h"

#define CRON_INTERVAL 1 //nie wieksze niz 30

extern int errno;
volatile unsigned int WORK = 1;
volatile unsigned int PRINT_TASKS = 0;
volatile unsigned int REREAD_TASKS = 0;

void handler(int signum)
{
	if(signum == SIGINT) {	// CTRL + C
		WORK = 0;
	}
	else if(signum == SIGUSR1) {	// kill(pid, SIGUSR1);
		REREAD_TASKS = 1;
	}
	else if(signum == SIGUSR2) {	// kill(pid, SIGUSR2);
		PRINT_TASKS = 1;
	}
}

int main(int argc, char *argv[])
{
	if(argc < 3) {
		fprintf(stderr, "Uzyj: %s <taskfile> <outfile>\n", argv[0]);
		return 0;
	}
	
	int taskFile = open(argv[1], O_RDONLY);
	if(taskFile == -1) {
		fprintf(stderr, "Nie udalo sie otworzyc pliku <taskfile> (%s).\n", argv[1]);
		return 0;
	}
	close(taskFile);

	int outFile = open(argv[2], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(outFile == -1) {
		fprintf(stderr, "Nie udalo sie otworzyc pliku <outfile> (%s).\n", argv[2]);
		return 0;
	}
	
	signal(SIGINT, handler);
	signal(SIGUSR1, handler);
	signal(SIGUSR2, handler);
	
	Task* tasks = NULL;
	
	//czytanie z pliku
	readTaskFile(&tasks, argv[1]);

	//sortowanie tablicy zadan
	sort(&tasks);
	printTasks(tasks);

	openlog("minicron", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

	//cron
	time_t atime;
	struct tm act_time;
	pid_t child_pid;

	//kill(getpid(), SIGUSR1); // W CELU WYWOŁANIA SIGUSR1
	//kill(getpid(), SIGUSR2); // W CELU WYWOŁANIA SIGUSR2

	while(WORK && tasks != NULL) {
		
		if(PRINT_TASKS) {
			PRINT_TASKS = 0;
			printTasksToSyslog(tasks);
		}
		
		if(REREAD_TASKS) {
			REREAD_TASKS = 0;
			removeAllTasks(&tasks);
			readTaskFile(&tasks, argv[1]);
			sort(&tasks);
			//printTasks(tasks);
			continue;
		}
		
		atime = time(NULL);
		act_time = *localtime(&atime);
		
		//printf("%d:%d - %d:%d\n", act_time.tm_hour, act_time.tm_min, tasks->hour, tasks->minute);
		
		if(tasks->hour == act_time.tm_hour && tasks->minute == act_time.tm_min) {
			if(tasks->command != NULL) {

				//printf("Wykonaj: %s\n", tasks->command);
				child_pid = fork();
				
				if(child_pid == 0) {
					//child
					
					//parsowanie polecenia
					char **args;
					int args_cnt = split_arguments(&args, tasks->command);
					//===
					
					//mode
					switch(tasks->mode)
					{
						case 0:
							dup2(outFile, STDOUT_FILENO);
							break;
						case 1: 
							dup2(outFile, STDERR_FILENO);
							break;
						case 2:
							dup2(outFile, STDOUT_FILENO);
							dup2(outFile, STDERR_FILENO);
							break;
					}
					
					//wykonanie polecenia
					syslog(LOG_NOTICE, "Execute: %s", tasks->command);
					int e = execvp(args[0], args);
					if(e == -1) {
						printf("%s\n", strerror(errno));
					}
					
					free(args);
				}
				
				int status;
				waitpid(child_pid, &status, 0);
				if(WIFEXITED(status)) 
				{ 
					int exit_status = WEXITSTATUS(status); 
					syslog(LOG_NOTICE, "\"%s\" return %d", tasks->command, exit_status);
				} 

			}

			removeFirstElement(&tasks);
			//printTasks(tasks);
		}
		
		
		sleep(CRON_INTERVAL); // czeka kilka sekund
	}

	close(outFile);
	closelog();
	
	return 0;
}