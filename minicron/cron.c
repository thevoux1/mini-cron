#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cron.h"

int number_of_arguments(char *string)
{
    int length = strlen(string);
    int i, number = 1;
    for(i=0;i<length;i++)
    {
        if(string[i]=='"')
            while(string[++i]!='"') { }

        else if(string[i]==' ')
            number++;
    }
    return number;
}

char* to_char_array(char c)
{
    char *result = malloc(2*sizeof(char));
    result[0] = c;
    result[1] = '\0';
    return result;
}

int split_arguments(char ***arguments, char *str)
{
    int noa = number_of_arguments(str);
    //char *arguments[noa];
	(*arguments) = (char**)malloc(noa * sizeof(char*));
    int i, arg_i = 0;
    (*arguments)[0] = (char*)malloc(100 * sizeof(char));
    strcpy((*arguments)[0], "");
    for(i=0;i<strlen(str);i++)
    {
        if(str[i]=='"')
        {
            do {
                strcat((*arguments)[arg_i], to_char_array(str[i]));
            } while(str[++i]!='"');
            strcat((*arguments)[arg_i], "\"");
        }

        else if(str[i]==' ')
        {
            (*arguments)[++arg_i] = (char*)malloc(100 * sizeof(char));
            strcpy((*arguments)[arg_i], "");
        }

        else
            strcat((*arguments)[arg_i], to_char_array(str[i]));
    }

	(*arguments)[++arg_i] = '\0';
	
    return noa;
}